_default:
    just --list

generate:
    go generate ./...

clean:
    rm -rf ./bin

program_name := "moncon.exe"

build:
    go build -ldflags "-H=windowsgui" -o ./bin/{{ program_name }} ./cmd/moncon

run *ARGS:
    go run ./cmd/moncon {{ ARGS }}

install:
    go install -ldflags "-H=windowsgui" ./cmd/moncon
