package util

import (
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

var Verbose bool

func Printf(cmd *cobra.Command, format string, i ...interface{}) {
	if Verbose {
		cmd.Printf(format, i...)
	}
}

func Println(cmd *cobra.Command, i ...interface{}) {
	if Verbose {
		cmd.Println(i...)
	}
}

func FlagWasSet(cmd *cobra.Command, name string) bool {
	out := false
	cmd.Flags().Visit(func(f *pflag.Flag) {
		if f.Name == name {
			out = true
		}
	})
	return out
}
