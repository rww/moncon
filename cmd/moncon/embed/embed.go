package embed

import (
	"bytes"
	_ "embed"
	"text/template"
)

//go:embed task.template.vbs
var taskTemplateString string
var taskTemplate = template.Must(template.New("task").Parse(taskTemplateString))

func TaskVbs(cmd string, args string) []byte {
	data := struct{ Cmd, Args string }{Cmd: cmd, Args: args}
	b := &bytes.Buffer{}
	if err := taskTemplate.Execute(b, data); err != nil {
		panic(err)
	}
	return b.Bytes()
}
