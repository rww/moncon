package config

import (
	"fmt"
	"os"
	"path"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var setCmd = &cobra.Command{
	Use:   "set [flags] <key> <value>",
	Short: "Set a configuration setting",
	Long:  `Set a configuration setting.`,
	Args:  cobra.ExactArgs(2),
	Run:   runSetCmd,
}

func initSetCmd(parent *cobra.Command) {
	parent.AddCommand(setCmd)
}

func runSetCmd(cmd *cobra.Command, args []string) {
	key := args[0]
	value := args[1]

	// TODO check if key is valid
	// TODO cast value to correct type
	viper.Set(key, value)

	// Write the configuration file
	err := viper.WriteConfig()
	if _, ok := err.(viper.ConfigFileNotFoundError); ok {
		// Create the configuration folder
		dir := path.Dir(viper.ConfigFileUsed())
		fmt.Println(dir)
		cobra.CheckErr(os.MkdirAll(dir, 0755))

		// Create the configuration file
		cobra.CheckErr(viper.SafeWriteConfig())
	}

	cmd.Printf("Set %s to %s\n", key, value)
}
