package config

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Manage configuration",
	Long:  `Manage the persistent configuration.`,
	Run:   runConfigCmd,
}

var (
	listConfigs bool
)

func InitConfigCmd(parent *cobra.Command) {
	configCmd.Flags().BoolVarP(&listConfigs, "list", "l", false, "list all configuration settings")
	initSetCmd(configCmd)

	parent.AddCommand(configCmd)
}

func runConfigCmd(cmd *cobra.Command, args []string) {
	if !listConfigs {
		cobra.CheckErr(cmd.Help())
		return
	}

	// Print all configuration settings
	settings := viper.AllSettings()
	for key, value := range settings {
		cmd.Printf("%s: %v\n", key, value)
	}
}
