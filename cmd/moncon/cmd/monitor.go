package cmd

import (
	"fmt"
	"strings"
	"time"

	"codeberg.org/rww/moncon"
)

type Monitor struct {
	vcp               moncon.VCP
	Name              string
	BrightnessRange   [2]uint32
	CurrentBrightness uint32
}

func GetMonitors() (monitors []Monitor, close func(), err error) {
	vcps, err := moncon.GetMonitors()
	if err != nil {
		return nil, nil, err
	}

	for i, v := range vcps {
		currentBrightness, maxBrightness, err := v.Feature(moncon.Luminance)
		if err != nil {
			return nil, nil, fmt.Errorf("setup could not get current brightness for monitor %d: %w", i, err)
		}

		model := "Unknown"
		if info := v.Info(); info != nil {
			model = info.Model
		}
		name := fmt.Sprintf("Monitor %d - %s", i, model)

		monitors = append(monitors, Monitor{
			vcp:               v,
			Name:              name,
			BrightnessRange:   [2]uint32{0, maxBrightness}, // default is full range
			CurrentBrightness: currentBrightness,
		})
	}

	close = func() {
		for _, v := range vcps {
			_ = v.Close()
		}
	}

	return monitors, close, nil
}

func (m *Monitor) SetBrightness(new uint32) error {
	if new == m.CurrentBrightness {
		return nil
	}

	err := m.vcp.SetFeature(moncon.Luminance, new)
	if err != nil {
		return fmt.Errorf("set brightness failed for %s: %w", m.Name, err)
	}

	currentBrightness, _, err := m.vcp.Feature(moncon.Luminance)
	if err != nil {
		return fmt.Errorf("set brightness could not get new brightness for %s: %w", m.Name, err)
	}
	m.CurrentBrightness = currentBrightness

	return nil
}

func (m *Monitor) SetBrightnessPercent(percent float64) error {
	if percent < 0 || percent > 100 {
		return fmt.Errorf("brightness percentage out of range")
	}
	low, high := m.BrightnessRange[0], m.BrightnessRange[1]
	new := uint32(float64(high-low) * percent / 100.0)
	return m.SetBrightness(new)
}

func (m *Monitor) SetBrightnessRange(low, high uint32) {
	if low > high {
		low, high = high, low
	}
	m.BrightnessRange = [2]uint32{low, high}
}

func (m *Monitor) Close() error { return m.vcp.Close() }

func selectByIndex(monitors []Monitor, index int) (Monitor, error) {
	if index < 0 || len(monitors) <= index {
		return Monitor{}, fmt.Errorf("index %d out of range", index)
	}
	return monitors[index], nil
}

func selectByName(monitors []Monitor, name string, caseSensitive bool) (Monitor, error) {
	for _, m := range monitors {
		if (m.Name == name) || (!caseSensitive && strings.EqualFold(m.Name, name)) {
			return m, nil
		}
	}
	return Monitor{}, fmt.Errorf("name %q not found", name)
}

// Brightness calculates the desired screen brightness for a time and location.
func Brightness(time time.Time, latitude, longitude float64) float64 {
	sunAngle, _ := moncon.SunPosition(time, latitude, longitude)

	const lowAngle = -10.0
	const highAngle = 20.0

	if sunAngle <= lowAngle {
		return 0
	} else if sunAngle >= highAngle {
		return 100
	} else {
		return float64(100) * (sunAngle - lowAngle) / (highAngle - lowAngle)
	}
}
