package task

import (
	"os"
	"path/filepath"

	"codeberg.org/rww/moncon/cmd/moncon/embed"
	"github.com/spf13/cobra"
)

var taskCmd = &cobra.Command{
	Use:   "task",
	Short: "manage the background task",
	Long:  `Manage the monitor brightness background task.`,
}

func InitTaskCmd(parent *cobra.Command) {
	initStatusCmd(taskCmd)
	initStartCmd(taskCmd)
	initRemoveCmd(taskCmd)

	parent.AddCommand(taskCmd)
}

const taskSchedulerPath = `\MonconBrightnessAdjustment`

func taskProgram() (action string, args string, err error) {
	exePath, err := os.Executable()
	if err != nil {
		return "", "", err
	}

	// Create a file in the ~/.moncon directory with the embedded .vbs file
	contents := embed.TaskVbs(exePath, "set --auto")
	home, err := os.UserHomeDir()
	if err != nil {
		return "", "", err
	}
	filePath := filepath.Join(home, ".moncon", "task.vbs")
	if err := os.WriteFile(filePath, contents, 0644); err != nil {
		// Fallback to the current executable path
		return exePath, "set --auto", nil
	}

	const wscriptPath = `%WINDIR%\System32\wscript.exe`
	filePathQuoted := `"` + filePath + `"`

	return wscriptPath, filePathQuoted, nil
}
