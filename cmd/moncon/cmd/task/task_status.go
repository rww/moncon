package task

import (
	"strings"

	"github.com/capnspacehook/taskmaster"
	"github.com/spf13/cobra"
)

var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "show the status of the background task",
	Long:  `Show the status of the monitor brightness background task.`,
	Run:   runStatusCmd,
}

func initStatusCmd(parent *cobra.Command) {
	parent.AddCommand(statusCmd)
}

func runStatusCmd(cmd *cobra.Command, args []string) {
	ts, err := taskmaster.Connect()
	cobra.CheckErr(err)
	defer ts.Disconnect()

	task, err := ts.GetRegisteredTask(taskSchedulerPath)
	if err != nil {
		if strings.Contains(err.Error(), "The system cannot find the file specified") {
			cmd.Println("Task not found")
			return
		}
		cobra.CheckErr(err)
	}
	defer task.Release()

	cmd.Printf("Task state is '%s'\n", task.State)
}
