package task

import (
	"strings"

	"codeberg.org/rww/moncon/cmd/moncon/util"
	"github.com/capnspacehook/taskmaster"
	"github.com/spf13/cobra"
)

var removeCmd = &cobra.Command{
	Use:   "remove",
	Short: "remove the background task",
	Long:  `Remove the monitor brightness adjustment background task.`,
	Run:   runRemoveCmd,
}

func initRemoveCmd(parent *cobra.Command) {
	parent.AddCommand(removeCmd)
}

func runRemoveCmd(cmd *cobra.Command, args []string) {
	ts, err := taskmaster.Connect()
	cobra.CheckErr(err)
	defer ts.Disconnect()

	task, err := ts.GetRegisteredTask(taskSchedulerPath)
	if err != nil {
		if strings.Contains(err.Error(), "The system cannot find the file specified") {
			cmd.Println("Task not found")
			return
		}
		cobra.CheckErr(err)
	}
	defer task.Release()

	if err := ts.DeleteTask(taskSchedulerPath); err != nil {
		cobra.CheckErr(err)
	}
	util.Println(cmd, "Task removed")
}
