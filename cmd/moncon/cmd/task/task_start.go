package task

import (
	"strings"
	"time"

	"codeberg.org/rww/moncon/cmd/moncon/util"
	"github.com/capnspacehook/taskmaster"
	"github.com/rickb777/date/period"
	"github.com/spf13/cobra"
)

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "start or update the background task",
	Long:  `Start or update the monitor brightness adjustment background task.`,
	Run:   runStartCmd,
}

var (
	periodMinutes  uint
	runOnLogon     bool
	updateIfExists bool
)

func initStartCmd(parent *cobra.Command) {
	startCmd.Flags().UintVarP(&periodMinutes, "period", "p", 15, "The period in minutes between brightness adjustments")
	startCmd.Flags().BoolVarP(&runOnLogon, "on-login", "L", false, "Run the task on logon of any user")
	startCmd.Flags().BoolVarP(&updateIfExists, "update", "u", false, "Update the task if it already exists")

	parent.AddCommand(startCmd)
}

func runStartCmd(cmd *cobra.Command, args []string) {
	ts, err := taskmaster.Connect()
	cobra.CheckErr(err)
	defer ts.Disconnect()

	exists := false
	task, err := ts.GetRegisteredTask(taskSchedulerPath)
	if err != nil {
		if !strings.Contains(err.Error(), "The system cannot find the file specified") {
			cobra.CheckErr(err)
		}
		// Task not found, will create it
		util.Println(cmd, "Task not found, will be created")
	} else {
		exists = true
		defer task.Release()
	}

	// Create or overwrite the task
	if !exists || updateIfExists {
		util.Println(cmd, "Creating/updating task...")
		def := ts.NewTaskDefinition()
		def.RegistrationInfo.Description = "Adjusts monitor brightness based on location and time of day."
		def.XMLText = ""

		if runOnLogon {
			def.AddTrigger(taskmaster.LogonTrigger{})
		}

		repetitionInterval, _ := period.NewOf(time.Duration(periodMinutes) * time.Minute)
		def.AddTrigger(taskmaster.TimeTrigger{
			TaskTrigger: taskmaster.TaskTrigger{
				Enabled:       true,
				StartBoundary: time.Now(),
				RepetitionPattern: taskmaster.RepetitionPattern{
					RepetitionInterval: repetitionInterval,
					RepetitionDuration: period.NewYMD(0, 0, 3256), // workaround for a bug in the taskmaster library: https://github.com/capnspacehook/taskmaster/issues/15#issuecomment-1205771923
				},
			},
		})

		action, args, err := taskProgram()
		cobra.CheckErr(err)
		def.AddAction(taskmaster.ExecAction{Path: action, Args: args})

		task, _, err = ts.CreateTask(taskSchedulerPath, def, true) // overwrite=true so ok is always true
		cobra.CheckErr(err)
		defer task.Release()
		util.Println(cmd, "Task created/updated")
	}

	// Make sure the task is enabled
	if !task.Enabled {
		util.Println(cmd, "Enabling task...")
		def := task.Definition
		def.Settings.Enabled = true
		task, err := ts.UpdateTask(taskSchedulerPath, def)
		cobra.CheckErr(err)
		defer task.Release()
		util.Println(cmd, "Task enabled")
	}

	cmd.Println("Task started")
}
