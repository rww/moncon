package cmd

import (
	"fmt"
	"os"
	"path"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"codeberg.org/rww/moncon/cmd/moncon/cmd/config"
	"codeberg.org/rww/moncon/cmd/moncon/cmd/task"
	"codeberg.org/rww/moncon/cmd/moncon/util"
)

var rootCmd = &cobra.Command{
	Use:     "moncon",
	Short:   "Monitor Control",
	Long:    `Control the brightness settings of connected monitors.`,
	Version: "0.0.0",
}

var (
	configFile        string
	doNotCreateConfig bool
)

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().BoolVarP(&util.Verbose, "verbose", "v", false, "print verbose output")
	rootCmd.PersistentFlags().StringVar(&configFile, "config", "", "config file (default is $HOME/.moncon/config.yaml)")
	rootCmd.PersistentFlags().BoolVar(&doNotCreateConfig, "no-config-file", false, "do not create a config file if one does not exist")

	initListCmd(rootCmd)
	initSetCmd(rootCmd)
	config.InitConfigCmd(rootCmd)
	task.InitTaskCmd(rootCmd)
}

func initConfig() {
	home, err := os.UserHomeDir()
	cobra.CheckErr(err)
	folder := path.Join(home, ".moncon")
	name := "config"
	fileType := "yaml"
	filePath := path.Join(folder, name+"."+fileType)

	// Set default values
	viper.SetDefault("longitude", float64(0))
	viper.SetDefault("latitude", float64(0))
	viper.SetTypeByDefaultValue(true)

	if configFile != "" {
		viper.SetConfigFile(configFile)
	} else {
		viper.SetConfigName(name)
		viper.SetConfigType(fileType)

		// Search for config file config.yaml in ~/.moncon/
		viper.AddConfigPath(folder)
	}

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			if doNotCreateConfig {
				cobra.CheckErr(fmt.Errorf("no config file found"))
			}

			// No config file found
			fmt.Println("No config file found, creating default file.")

			// Create the configuration folder
			cobra.CheckErr(os.MkdirAll(folder, 0755))

			// Create the configuration file
			cobra.CheckErr(viper.SafeWriteConfigAs(filePath))
		} else {
			cobra.CheckErr(err)
		}
	}
}
