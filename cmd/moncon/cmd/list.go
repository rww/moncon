package cmd

import (
	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List the available monitors",
	Args:  cobra.NoArgs,
	Run:   runListCmd,
}

func initListCmd(parent *cobra.Command) {
	parent.AddCommand(listCmd)
}

func runListCmd(cmd *cobra.Command, args []string) {
	monitors, close, err := GetMonitors()
	cobra.CheckErr(err)
	defer close()

	for _, m := range monitors {
		cmd.Println(m.Name)
	}
}
