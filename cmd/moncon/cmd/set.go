package cmd

import (
	"os"
	"time"

	"codeberg.org/rww/moncon/cmd/moncon/util"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var setCmd = &cobra.Command{
	Use:   "set [flags]",
	Short: "Set the brightness of all monitors or a specific monitor",
	Long:  `Set the brightness of all monitors or a specific monitor by index or name.`,
	Args:  cobra.NoArgs,
	Run:   runSetCmd,
}

var (
	autoBrightness bool
	setBrightness  float64
	monitorIndex   int
	monitorName    string
	caseSensitive  bool
)

func initSetCmd(parent *cobra.Command) {
	setCmd.Flags().BoolVarP(&autoBrightness, "auto", "a", false, "set brightness based on location and time")
	setCmd.Flags().Float64VarP(&setBrightness, "brightness", "b", 0, "set brightness to a specific percentage")
	setCmd.Flags().IntVarP(&monitorIndex, "index", "i", 0, "select monitor by index (0 and up)")
	setCmd.Flags().StringVarP(&monitorName, "name", "n", "", "select monitor by name")
	setCmd.Flags().BoolVarP(&caseSensitive, "case-sensitive", "C", false, "match monitor name case-sensitively")

	setCmd.Flags().SortFlags = false

	setCmd.MarkFlagsOneRequired("auto", "brightness")
	setCmd.MarkFlagsMutuallyExclusive("index", "name")

	parent.AddCommand(setCmd)
}

func runSetCmd(cmd *cobra.Command, args []string) {
	// Determine the brightness to use
	brightness := setBrightness // Default to the set brightness (may not be set)
	if autoBrightness {
		// Get location from config
		latitude := viper.GetFloat64("latitude")
		longitude := viper.GetFloat64("longitude")

		// Calculate brightness
		brightness = Brightness(time.Now(), latitude, longitude)
	}

	// Find the selected monitor(s)
	monitors, close, err := GetMonitors()
	cobra.CheckErr(err)
	defer close()
	selected, err := selectMonitor(cmd, monitors)
	cobra.CheckErr(err)

	// Set the brightness
	noIssues := true
	for _, m := range selected {
		err := m.SetBrightnessPercent(brightness)
		if err != nil {
			noIssues = false
			cmd.PrintErrf("Failed to set brightness on monitor %q: %v\n", m.Name, err)
		} else {
			util.Printf(cmd, "Set brightness on monitor %q to %g%%\n", m.Name, brightness)
		}
	}
	if !noIssues {
		os.Exit(1)
	}
}

func selectMonitor(cmd *cobra.Command, selected []Monitor) ([]Monitor, error) {
	if util.FlagWasSet(cmd, "index") {
		m, err := selectByIndex(selected, monitorIndex)
		return []Monitor{m}, err
	}

	if util.FlagWasSet(cmd, "name") {
		m, err := selectByName(selected, monitorName, caseSensitive)
		return []Monitor{m}, err
	}

	return selected, nil
}
