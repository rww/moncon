package main

import (
	"codeberg.org/rww/moncon/cmd/moncon/cmd"
)

func main() {
	_ = cmd.Execute()
}
