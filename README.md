# Moncon

Moncon is a small CLI program to do automatic monitor dimming based on the sun's position. It is currently only compatible with Windows.

It is installed with `go install codeberg.org/rww/moncon/cmd/moncon@latest` and then run with `moncon`.
