package moncon

import (
	"errors"
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type VCPInfo struct {
	ProtocolClass     string
	Type              string
	Commands          []Code
	Codes             map[Code]CodeInfo
	Model             string
	MCCSVer           string
	Windows           map[string]string
	Renames           map[Code]string
	NonStandardFields map[string]string
}

type CodeInfo struct {
	UseDiscrete    bool
	DiscreteValues []uint32
}

var ErrKeyStringNotFound = errors.New("key string not found")
var ErrMissingFirstBrace = errors.New("missing opening brace")
var ErrMissingClosingBrace = errors.New("missing closing brace")

var topLevelItemRegex = regexp.MustCompile(`(\w+)\(`)
var codesRegex = regexp.MustCompile(`([0-9A-Fa-f]{1,2})(?:\(([0-9A-Fa-f ]*)\))?`)
var windowKeyRegex = regexp.MustCompile(`window\d+`)
var renamesRegex = regexp.MustCompile(`([0-9A-Fa-f]{1,2})\(([^\)]*)\)`)

func parseCabilitiesString(s string) (*VCPInfo, error) {
	info := VCPInfo{
		Commands:          make([]Code, 0),
		Codes:             map[Code]CodeInfo{},
		Windows:           map[string]string{},
		Renames:           map[Code]string{},
		NonStandardFields: map[string]string{},
	}

	top, err := getTopLevelItems(s)
	if err != nil {
		return nil, fmt.Errorf("parsing capability string: top level %w", err)
	}

	if v, contains := top["prot"]; contains {
		info.ProtocolClass = v
		delete(top, "prot")
	}
	if v, contains := top["type"]; contains {
		info.Type = v
		delete(top, "type")
	}
	if v, contains := top["cmds"]; contains {
		info.Commands, err = parseCommands(v)
		if err != nil {
			return nil, fmt.Errorf("parsing capability string: cmds %w", err)
		}
		delete(top, "cmds")
	}
	if v, contains := top["vcp"]; contains {
		info.Codes, err = parseCodes(v)
		if err != nil {
			return nil, fmt.Errorf("parsing capability string: vcp %w", err)
		}
		delete(top, "vcp")
	}
	if v, contains := top["model"]; contains {
		info.Model = v
		delete(top, "model")
	}
	if v, contains := top["mccs_ver"]; contains {
		info.MCCSVer = v
		delete(top, "mccs_ver")
	}
	for k, v := range top {
		if windowKeyRegex.MatchString(k) {
			info.Windows[k] = v
			delete(top, k)
		}
	}
	if v, contains := top["vcpname"]; contains {
		info.Renames, err = parseRenames(v)
		if err != nil {
			return nil, fmt.Errorf("parsing capability string: vcpname %w", err)
		}
		delete(top, "vcpname")
	}

	info.NonStandardFields = top

	return &info, nil
}

// getTopLevelItems finds all named opening brackets, and returns a map of the key(value) from each one. Returns nil if there is an error.
func getTopLevelItems(in string) (map[string]string, error) {
	out := map[string]string{}
	for m := topLevelItemRegex.FindStringSubmatchIndex(in); len(m) >= 4; m = topLevelItemRegex.FindStringSubmatchIndex(in) {
		key := in[m[2]:m[3]]
		openBrace := m[1] - 1
		closeBrace, err := findClosingBrace(in, openBrace)
		if err != nil {
			return nil, err
		}
		out[key] = in[(openBrace + 1):closeBrace]
		in = strings.Replace(in, in[m[0]:(closeBrace+1)], "", 1)
	}

	return out, nil
}

func findClosingBrace(in string, openingBrace int) (closingBrace int, err error) {
	if in[openingBrace] != '(' { // Return error if not found
		return -1, ErrMissingFirstBrace
	}
	count := 1
	for i := openingBrace + 1; i < len(in); i++ {
		c := in[i]
		if c == '(' {
			count++ // Count up for opening braces
		} else if c == ')' {
			count-- // Count down for closing braces
		}
		if count <= 0 { // Until count is at zero
			return i, nil
		}
	}
	return -1, ErrMissingClosingBrace
}

func parseCommands(s string) ([]Code, error) {
	codes := make([]Code, 0)
	codeStrings := strings.Split(s, " ")
	for _, cs := range codeStrings {
		if len(cs) == 0 {
			continue // Skip blanks (caused by multiple consecutive spaces)
		}
		c, err := strconv.ParseUint(cs, 16, 8)
		if err != nil {
			return nil, fmt.Errorf("parsing command '%s': %w", cs, err)
		}
		codes = append(codes, Code(c))
	}
	sort.Slice(codes, func(i, j int) bool { return codes[i] < codes[j] })
	return codes, nil
}

func parseCodes(s string) (map[Code]CodeInfo, error) {
	codes := map[Code]CodeInfo{}
	matches := codesRegex.FindAllStringSubmatch(s, -1)
	for _, m := range matches {
		if len(m[1]) == 0 {
			continue // Skip blanks (caused by multiple consecutive spaces)
		}
		c, err := strconv.ParseUint(m[1], 16, 8)
		if err != nil {
			return nil, fmt.Errorf("parsing code '%s': %w", m[1], err)
		}
		codeInfo := CodeInfo{
			UseDiscrete:    false,
			DiscreteValues: nil,
		}

		if len(m[2]) > 0 { // Found a list of discrete values
			codeInfo.UseDiscrete = true
			codeInfo.DiscreteValues = make([]uint32, 0)
			for _, ds := range strings.Split(m[2], " ") {
				if len(ds) == 0 {
					continue // Skip blanks (caused by multiple consecutive spaces)
				}
				d, err := strconv.ParseUint(ds, 16, 32)
				if err != nil {
					return nil, fmt.Errorf("parsing code discrete value '%s': %w", ds, err)
				}
				codeInfo.DiscreteValues = append(codeInfo.DiscreteValues, uint32(d))
			}
			sort.Slice(codeInfo.DiscreteValues, func(i, j int) bool { return codeInfo.DiscreteValues[i] < codeInfo.DiscreteValues[j] })
		}

		codes[Code(c)] = codeInfo
	}
	return codes, nil
}

func parseRenames(s string) (map[Code]string, error) {
	renames := map[Code]string{}
	matches := renamesRegex.FindAllStringSubmatch(s, -1)
	for _, m := range matches {
		c, err := strconv.ParseUint(m[1], 16, 8)
		if err != nil {
			return nil, fmt.Errorf("parsing code '%s': %w", m[1], err)
		}
		renames[Code(c)] = m[2]
	}
	return renames, nil
}

// String returns a string representing a VCPInfo structure.
func (vi *VCPInfo) String() string {
	var b strings.Builder

	fmt.Fprintln(&b, "Protocol Class:", vi.ProtocolClass)
	fmt.Fprintln(&b, "Type:", vi.Type)
	fmt.Fprintln(&b, "Commands:", vi.Commands)
	fmt.Fprintln(&b, "Codes:", vi.Codes)
	fmt.Fprintln(&b, "Model:", vi.Model)
	fmt.Fprintln(&b, "MCCSVer:", vi.MCCSVer)
	fmt.Fprintln(&b, "Windows:", vi.Windows)
	fmt.Fprintln(&b, "Renames:", vi.Renames)
	fmt.Fprint(&b, "Non-standard Fields:", vi.NonStandardFields)

	return b.String()
}

// String returns a string representing a CodeInfo structure.
func (ci CodeInfo) String() string {
	if !ci.UseDiscrete {
		return ""
	}
	return fmt.Sprintf("%v", ci.DiscreteValues)
}
