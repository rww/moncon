package moncon

import (
	"math"
	"time"
)

const (
	unixEpochJulianDate float64 = 2440587.5
	secondsPerDay       float64 = 24 * 60 * 60
)

func julianDay(t time.Time) float64 { return (float64(t.Unix()) / secondsPerDay) + unixEpochJulianDate }

func rad(deg float64) float64 { return (math.Pi / 180.0) * deg }

func deg(rad float64) float64 { return (180.0 / math.Pi) * rad }

func SunPosition(t time.Time, latitude, longitude float64) (elevationAngle, azimuthAngle float64) {
	const (
		centuryJulianDate float64 = 2451545 // 2000-Jan-01 at Noon
		julianCenturyDays         = 36525
		twoPi                     = 2 * math.Pi
	)

	jd := julianDay(t)
	jcent := (jd - centuryJulianDate) / julianCenturyDays

	sunGeomMeanLong := polynomial(jcent, 4.895063168412976, 628.3319667861393, 5.291838292046807e-6) // rad
	sunGeomMeanLong = math.Mod(sunGeomMeanLong, twoPi)
	sunGeomMeanAnom := polynomial(jcent, 6.24006014122498, 628.30195515152, -2.68257106031528e-6) // rad
	eccentEarthOrbit := polynomial(jcent, 0.016708634, -0.000042037, -0.0000001267)
	sunEqOfCenter := (math.Sin(3*sunGeomMeanAnom)*5.04400153826361e-6 +
		math.Sin(2*sunGeomMeanAnom)*polynomial(jcent, 3.48943677351226e-4, -1.76278254451427e-6) +
		math.Sin(sunGeomMeanAnom)*polynomial(jcent, 3.34161087652685e-2, -8.40725100685669e-5, -2.44346095279206e-7))
	sunTrueLong := sunGeomMeanLong + sunEqOfCenter                                                                        // rad
	sunAppLong := sunTrueLong - 9.93092344384773e-5 - 8.3426738245329e-5*math.Sin(2.18235969669371-33.757041381353*jcent) // rad

	meanObliqueEcliptic := polynomial(jcent, 4.09092804222329e-1, -2.26965524811429e-4, -2.86040071854626e-9, 8.78967203851589e-9) // rad

	obliqueCorrection := meanObliqueEcliptic + 4.46804288510548e-5*math.Cos(2.18235969669371-33.757041381353*jcent) // rad
	sunDeclination := math.Asin(math.Sin(obliqueCorrection) * math.Sin(sunAppLong))                                 // rad
	y := powInt(math.Tan(obliqueCorrection/2), 2)
	eqOfTime := 4 * deg(y*math.Sin(2*sunGeomMeanLong)-2*eccentEarthOrbit*math.Sin(sunGeomMeanAnom)+4*eccentEarthOrbit*y*math.Sin(sunGeomMeanAnom)*math.Cos(2*sunGeomMeanLong)-0.5*y*y*math.Sin(4*sunGeomMeanLong)-1.25*eccentEarthOrbit*eccentEarthOrbit*math.Sin(2*sunGeomMeanAnom)) // minutes
	hourAngle := 0.25*(minutesIntoUTCDay(t)+eqOfTime) + longitude - 180                                                                                                                                                                                                               // deg
	for hourAngle < -180 {
		hourAngle += 360
	}
	for hourAngle >= 180 {
		hourAngle -= 360
	}
	solarZenithAngle := math.Acos(math.Sin(rad(latitude))*math.Sin(sunDeclination) + math.Cos(rad(latitude))*math.Cos(sunDeclination)*math.Cos(rad(hourAngle)))
	solarElevationAngle := (math.Pi / 2) - solarZenithAngle
	atmosphericRefraction := atmosphericRefraction(solarElevationAngle)
	solarAzimuthAngle := solarAzimuthAngle(hourAngle, latitude, solarZenithAngle, sunDeclination)

	return deg(solarElevationAngle + atmosphericRefraction), deg(solarAzimuthAngle)
}

func minutesIntoUTCDay(t time.Time) float64 {
	beginningUTCDay := time.Date(t.UTC().Year(), t.UTC().Month(), t.UTC().Day(), 0, 0, 0, 0, time.UTC)
	d := t.Sub(beginningUTCDay)
	return d.Minutes()
}

func atmosphericRefraction(sunElevAngle float64) float64 {
	tangentRatio := math.Tan(sunElevAngle)
	sunElevAngleDeg := deg(sunElevAngle)
	var deg float64
	switch {
	case sunElevAngleDeg > 85:
		deg = 0
	case sunElevAngleDeg > 5:
		deg = 58.1/tangentRatio - 0.07/powInt(tangentRatio, 3) + 0.000086/powInt(tangentRatio, 5)
	case sunElevAngleDeg > -0.575:
		deg = 1735 + sunElevAngleDeg*(-518.2+sunElevAngleDeg*(103.4+sunElevAngleDeg*(-12.79+sunElevAngleDeg*0.711)))
	default:
		deg = -20.772 / tangentRatio
	}
	return rad(deg / 3600)
}

func solarAzimuthAngle(hourAngle, latitude, solarZenithAngle, sunDeclination float64) float64 {
	x := math.Acos(((math.Sin(rad(latitude)) * math.Cos(solarZenithAngle)) - math.Sin(sunDeclination)) / (math.Cos(rad(latitude)) * math.Sin(solarZenithAngle)))
	if hourAngle > 0 {
		return math.Mod(x+math.Pi, 2*math.Pi)
	} else {
		return math.Mod((3*math.Pi)-x, 2*math.Pi)
	}
}

// powInt evaluates x^n, where n is an integer.
func powInt(x float64, n int) float64 {
	if n < 0 {
		return 1.0 / powInt(x, -n)
	}

	out := float64(1)
	for n != 0 {
		if (n & 1) != 0 { // n is odd
			out *= x
		}
		n = n >> 1
		x *= x
	}
	return out
}

// polynomial evaluates a polynomial with coefficients [b...] at x using Horner's method.
func polynomial(x float64, b ...float64) float64 {
	if len(b) == 0 {
		return 0
	}
	out := b[len(b)-1]
	for i := len(b) - 2; i >= 0; i-- {
		out = b[i] + (out * x)
	}
	return out
}

// secantRootFind iterates nIterations times towards a root of f, using x0 and x1 as starting points.
//
// It uses the secant method.
func secantRootFind(f func(float64) float64, x0 float64, x1 float64, nIterations int) float64 {
	f0, f1 := f(x0), float64(0)
	x2 := x1
	for i := 0; i < nIterations; i++ {
		f1 = f(x1)
		x2 = x1 - f1*(x1-x0)/(f1-f0)
		x0, x1 = x1, x2
		f0 = f1
	}
	return x2
}

// func SunRiseSunset(date time.Time, latitude, longitude float64) (rise, set time.Time) {
// 	f := func(x float64) float64 {
// 		ev, _ := SunPosition(t, latitude, longitude)
// 		return ev
// 	}
// }
