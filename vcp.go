package moncon

type VCP interface {
	// Info returns the monitor's capability info, parsed from its capability string.
	Info() *VCPInfo

	// Feature queries a monitor for a code's current and max values.
	Feature(code Code) (value, maximum uint32, err error)

	// SetFeature sets a monitor's code to a value.
	SetFeature(code Code, value uint32) error

	// Close tidies up after a monitor is no longer needed.
	Close() error
}
