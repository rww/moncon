package moncon

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestParseCabilitiesString(t *testing.T) {
	var capabilitiesStringTests = []struct {
		input  string
		result VCPInfo
		err    error
	}{
		{input: "prot(display) type(lcd) model(xxxxx) cmds(01 02 A1) vcp(02 03 10 12 C8 DC(00 01 02 03 07) DF) mccs_ver(2.2) window1(type(PIP) area(25 25 1895 1175) max(640 480) min(10 10) window(10)) vcpname(10(Brightness) FF(A weird long name))",
			result: VCPInfo{
				ProtocolClass: "display",
				Type:          "lcd",
				Model:         "xxxxx",
				Commands:      []Code{0x01, 0x02, 0xA1},
				Codes: map[Code]CodeInfo{
					0x02: {false, nil},
					0x03: {false, nil},
					0x10: {false, nil},
					0x12: {false, nil},
					0xC8: {false, nil},
					0xDC: {true, []uint32{0, 1, 2, 3, 7}},
					0xDF: {false, nil},
				},
				MCCSVer:           "2.2",
				Windows:           map[string]string{"window1": "type(PIP) area(25 25 1895 1175) max(640 480) min(10 10) window(10)"},
				Renames:           map[Code]string{0x10: "Brightness", 0xFF: "A weird long name"},
				NonStandardFields: map[string]string{},
			},
			err: nil,
		},
		{input: "(prot(monitor)type(LCD)model(VG248)cmds(01 02 03 07 0C F3)vcp(02 04 05 08 0B 0C 10 12 14(05 06 08 0B) 16 18 1A 60(03 11 0F) 62 A8 AC AE B6 C6 C8 C9 CC(01 02 03 04 05 06 07 08 09 0A 0C 0D 11 12 14 1A 1E 1F 23 72 73 ) D6(01 04) DF)mccs_ver(2.2)asset_eep(32)mpu(01)mswhql(1))",
			result: VCPInfo{
				ProtocolClass: "monitor",
				Type:          "LCD",
				Model:         "VG248",
				Commands:      []Code{0x01, 0x02, 0x03, 0x07, 0x0C, 0xF3},
				Codes: map[Code]CodeInfo{
					0x02: {false, nil},
					0x04: {false, nil},
					0x05: {false, nil},
					0x08: {false, nil},
					0x0B: {false, nil},
					0x0C: {false, nil},
					0x10: {false, nil},
					0x12: {false, nil},
					0x14: {true, []uint32{0x05, 0x06, 0x08, 0x0B}},
					0x16: {false, nil},
					0x18: {false, nil},
					0x1A: {false, nil},
					0x60: {true, []uint32{0x03, 0x0F, 0x11}},
					0x62: {false, nil},
					0xA8: {false, nil},
					0xAC: {false, nil},
					0xAE: {false, nil},
					0xB6: {false, nil},
					0xC6: {false, nil},
					0xC8: {false, nil},
					0xC9: {false, nil},
					0xCC: {true, []uint32{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0C, 0x0D, 0x11, 0x12, 0x14, 0x1A, 0x1E, 0x1F, 0x23, 0x72, 0x73}},
					0xD6: {true, []uint32{1, 4}},
					0xDF: {false, nil},
				},
				MCCSVer:           "2.2",
				Windows:           map[string]string{},
				Renames:           map[Code]string{},
				NonStandardFields: map[string]string{"asset_eep": "32", "mpu": "01", "mswhql": "1"},
			},
			err: nil,
		},
		{input: "(prot(monitor)type(lcd)model(FALCON)cmds(01 02 03 07 0C 4E F3 E3)vcp(02 04 05 08 0B 0C 10 12 14(01 04 05 06 07 08 0A 0B) 16 18 1A 6C 6E 70 AC AE B6 C0 C6 C8 C9 CA CC(00 02 03 04 05 08 09 0A 0D) D6(01 04) DC(00 01 02 03 04) DF 60(01 03) 62 8D FF)mswhql(1)mccs_ver(2.0)asset_eep(32)mpu_ver(01))",
			result: VCPInfo{
				ProtocolClass: "monitor",
				Type:          "lcd",
				Model:         "FALCON",
				Commands:      []Code{0x01, 0x02, 0x03, 0x07, 0x0C, 0x4E, 0xE3, 0xF3},
				Codes: map[Code]CodeInfo{
					0x02: {false, nil},
					0x04: {false, nil},
					0x05: {false, nil},
					0x08: {false, nil},
					0x0B: {false, nil},
					0x0C: {false, nil},
					0x10: {false, nil},
					0x12: {false, nil},
					0x14: {true, []uint32{0x01, 0x04, 0x05, 0x06, 0x07, 0x08, 0x0A, 0x0B}},
					0x16: {false, nil},
					0x18: {false, nil},
					0x1A: {false, nil},
					0x60: {true, []uint32{0x01, 0x03}},
					0x62: {false, nil},
					0x6C: {false, nil},
					0x6E: {false, nil},
					0x70: {false, nil},
					0x8D: {false, nil},
					0xAC: {false, nil},
					0xAE: {false, nil},
					0xB6: {false, nil},
					0xC0: {false, nil},
					0xC6: {false, nil},
					0xC8: {false, nil},
					0xC9: {false, nil},
					0xCA: {false, nil},
					0xCC: {true, []uint32{0x00, 0x02, 0x03, 0x04, 0x05, 0x08, 0x09, 0x0A, 0x0D}},
					0xD6: {true, []uint32{0x01, 0x04}},
					0xDC: {true, []uint32{0x00, 0x01, 0x02, 0x03, 0x04}},
					0xDF: {false, nil},
					0xFF: {false, nil},
				},
				MCCSVer:           "2.0",
				Windows:           map[string]string{},
				Renames:           map[Code]string{},
				NonStandardFields: map[string]string{"asset_eep": "32", "mpu_ver": "01", "mswhql": "1"},
			},
			err: nil,
		},
	}
	for _, test := range capabilitiesStringTests {
		t.Run(fmt.Sprintf("'%s'", test.input), func(t *testing.T) {
			want := test.result
			got, err := parseCabilitiesString(test.input)
			if err != nil && test.err == nil {
				t.Fatalf("bad error: got '%v', want '%v'", err, test.err)
			}
			if diff := cmp.Diff(want, *got); diff != "" {
				t.Errorf("result mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func TestGetTopLevelItems(t *testing.T) {
	var tests = []struct {
		input  string
		result map[string]string
	}{
		{input: "prot(display) window1(type (PIP) area(25 25 1895 1175) max(640 480) min(10 10) window(10)) vcpname(10(Brightness))",
			result: map[string]string{"prot": "display", "window1": "type (PIP) area(25 25 1895 1175) max(640 480) min(10 10) window(10)", "vcpname": "10(Brightness)"},
		},
		{input: "prot(display)type(lcd)model(xxxxx)cmds(xxxxx)vcp(02 03 10 12 C8 DC(00 01 02 03 07) DF)mccs_ver(2.2)window1(type(PIP)area(25 25 1895 1175)max(640 480)min(10 10)window(10))vcpname(10(Brightness))",
			result: map[string]string{"prot": "display", "type": "lcd", "model": "xxxxx", "cmds": "xxxxx", "mccs_ver": "2.2", "vcp": "02 03 10 12 C8 DC(00 01 02 03 07) DF", "window1": "type(PIP)area(25 25 1895 1175)max(640 480)min(10 10)window(10)", "vcpname": "10(Brightness)"},
		},
		{input: "prot(display) mccs_ver(2.2", result: nil},
		{input: "prot(display)vcp(02 03 10 12 C8 DC(00 01 02 03 07 DF)mccs_ver(2.2)", result: nil},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("'%s'", test.input), func(t *testing.T) {
			want := test.result
			got, err := getTopLevelItems(test.input)
			if err != nil && want != nil {
				t.Fatalf("unexpected error: %s", err)
			}
			if diff := cmp.Diff(want, got); diff != "" {
				t.Errorf("result mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
