package moncon

import (
	"fmt"
	"math"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestPowInt(t *testing.T) {
	tests := []struct {
		x      float64
		n      int
		result float64
	}{
		{0.0, -1, math.Inf(1)},
		{0.0, 0, 1.0},
		{0.0, 1, 0.0},

		{1.0, -1, 1.0},
		{1.0, 0, 1.0},
		{1.0, 1, 1.0},

		{-1.0, -1, -1.0},
		{-1.0, 0, 1.0},
		{-1.0, 1, -1.0},

		{2.0, -2, 0.25},
		{2.0, -1, 0.5},
		{2.0, 0, 1.0},
		{2.0, 1, 2.0},
		{2.0, 2, 4.0},
		{2.0, 5, 32.0},

		{-2.0, -2, 0.25},
		{-2.0, -1, -0.5},
		{-2.0, 0, 1.0},
		{-2.0, 1, -2.0},
		{-2.0, 2, 4.0},
		{-2.0, 5, -32.0},

		{29.3415, 3, 25260.790462248375},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("x=%+v,n=%d", test.x, test.n), func(t *testing.T) {
			want := test.result
			got := powInt(test.x, test.n)
			if !equalFloats(got, want) {
				t.Errorf("got %+v, want %+v", got, want)
			}
		})
	}
}

func TestPolynomial(t *testing.T) {
	tests := []struct {
		x      float64
		b      []float64
		result float64
	}{
		{3.333, []float64{}, 0},
		{3.333, []float64{1}, 1},
		{3.333, []float64{1, 1}, 4.333},
		{3.333, []float64{1, 1, 1}, 15.441889},
		{3.333, []float64{1, 1, 1, 1}, 52.467816037},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("x=%+v,b=%+v", test.x, test.b), func(t *testing.T) {
			want := test.result
			got := polynomial(test.x, test.b...)
			if !equalFloats(got, want) {
				t.Errorf("got %+v, want %+v", got, want)
			}
		})
	}
}

var testsSunPosition = []struct {
	time            time.Time
	lat             float64
	long            float64
	resultElevation float64
	resultAzimuth   float64
}{
	{time.Date(2022, time.May, 21, 8, 12, 0, 0, time.UTC), 49.246292, -123.116226, -20.5286330749761, 0.7357419679880760},
	{time.Date(1999, time.February, 15, 17, 54, 0, 0, time.UTC), -20.2, -88, 81.904808207024, 21.5836828382011},
	{time.Date(2023, time.November, 11, 2, 36, 0, 0, time.UTC), 83, 179.9, -12.1069410185276, 221.683840682122},
	{time.Date(2022, time.May, 21, 3, 54, 0, 0, time.UTC), 49.246292, -123.116226, 0.0158333732309831, 302.647819738948},
}

func TestSunPosition(t *testing.T) {
	for _, test := range testsSunPosition {
		t.Run(fmt.Sprintf("t=%s,lat=%+v,long=%+v", test.time, test.lat, test.long), func(t *testing.T) {
			wantElevation, wantAzimuth := test.resultElevation, test.resultAzimuth
			gotElevation, gotAzimuth := SunPosition(test.time, test.lat, test.long)
			if !equalFloats(gotElevation, wantElevation) {
				t.Errorf("bad elevation: got %+v, want %+v", gotElevation, wantElevation)
			}
			if !equalFloats(gotAzimuth, wantAzimuth) {
				t.Errorf("bad azimuth: got %+v, want %+v", gotAzimuth, wantAzimuth)
			}
		})
	}
}

func BenchmarkSunPosition(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SunPosition(time.Date(2022, time.May, 21, 1, 12, 0, 0, time.Local), 49.246292, -123.116226)
	}
}

func equalFloats(x, y float64) bool {
	return cmp.Equal(x, y, cmpopts.EquateNaNs(), cmpopts.EquateApprox(1e-10, 0))
}

func TestSecantRootFind(t *testing.T) {
	tests := []struct {
		desc   string
		f      func(float64) float64
		x0     float64
		x1     float64
		n      int
		result float64
	}{
		{"x^2-612", func(x float64) float64 { return x*x - 612 }, 10, 30, 5, 24.738633748750722},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("f=%s,x0=%v,x1=%v,n=%d", test.desc, test.x0, test.x1, test.n), func(t *testing.T) {
			want := test.result
			got := secantRootFind(test.f, test.x0, test.x1, test.n)
			if !equalFloats(got, want) {
				t.Errorf("got %+v, want %+v", got, want)
			}
		})
	}
}
