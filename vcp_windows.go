package moncon

import (
	"strings"
	"sync"
	"unsafe"

	"golang.org/x/sys/windows"
)

const (
	validReturn = 1
)

type (
	dword         uint32
	devConHandle  windows.Handle
	monitorHandle windows.Handle
	lparam        uintptr
	lresult       uintptr
	wchar         uint16
)

// http://msdn.microsoft.com/en-us/library/windows/desktop/dd162897.aspx
type rectangle struct {
	Left, Top, Right, Bottom int32
}

type monitorInfo struct {
	Handle        monitorHandle
	DeviceContext devConHandle
	Rectangle     rectangle
}

type monitorCache struct {
	mu        sync.RWMutex
	HMonitors []monitorInfo
}

var (
	moduser32 = windows.MustLoadDLL("user32.dll")
	dxva2     = windows.MustLoadDLL("Dxva2.dll")

	procEnumDisplayMonitors                     = moduser32.MustFindProc("EnumDisplayMonitors")
	procGetNumberOfPhysicalMonitorsFromHMONITOR = dxva2.MustFindProc("GetNumberOfPhysicalMonitorsFromHMONITOR")
	procGetPhysicalMonitorsFromHMONITOR         = dxva2.MustFindProc("GetPhysicalMonitorsFromHMONITOR")
	procDestroyPhysicalMonitor                  = dxva2.MustFindProc("DestroyPhysicalMonitor")
	procGetCapabilitiesStringLength             = dxva2.MustFindProc("GetCapabilitiesStringLength")
	procCapabilitiesRequestAndCapabilitiesReply = dxva2.MustFindProc("CapabilitiesRequestAndCapabilitiesReply")
	procGetVCPFeatureAndVCPFeatureReply         = dxva2.MustFindProc("GetVCPFeatureAndVCPFeatureReply")
	procSetVCPFeature                           = dxva2.MustFindProc("SetVCPFeature")
)

var monCache monitorCache

func (mc *monitorCache) Add(hmi monitorInfo) {
	monCache.mu.Lock()
	defer monCache.mu.Unlock()
	monCache.HMonitors = append(monCache.HMonitors, hmi)
}

func (mc *monitorCache) Reset() {
	monCache.mu.Lock()
	defer monCache.mu.Unlock()
	monCache.HMonitors = nil
}

func (mc *monitorCache) GetAll() []monitorInfo {
	monCache.mu.RLock()
	defer monCache.mu.RUnlock()
	hmInfo := make([]monitorInfo, len(monCache.HMonitors))
	copy(hmInfo, monCache.HMonitors)
	return hmInfo
}

func monitorEnumProc(hm monitorHandle, hdc devConHandle, r *rectangle, lp lparam) lresult {
	monCache.Add(monitorInfo{
		Handle:        hm,
		DeviceContext: hdc,
		Rectangle:     *r,
	})
	return 1 // True, to continue enumeration
}

func updateDisplayMonitors() ([]monitorInfo, error) {
	monCache.Reset()
	err := enumDisplayMonitors(0, nil, windows.NewCallback(monitorEnumProc), 0)
	return monCache.GetAll(), err
}

// enumDisplayMonitors calls the Windows API to run a callback function for each Monitor in
// a given display area.
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-enumdisplaymonitors
func enumDisplayMonitors(hdc devConHandle, clip *rectangle, fnEnum uintptr, dwData lparam) error {
	ret, _, err := procEnumDisplayMonitors.Call(
		uintptr(hdc),
		uintptr(unsafe.Pointer(clip)),
		fnEnum,
		uintptr(dwData),
	)
	if ret != validReturn {
		return err
	}
	return nil
}

func GetMonitors() ([]VCP, error) {
	vcps := make([]VCP, 0)
	hmonitors, err := updateDisplayMonitors()
	if err != nil {
		return nil, err
	}
	for _, hm := range hmonitors {
		n, err := getNumberOfPhysicalMonitorsFromHMONITOR(hm.Handle)
		if err != nil {
			return nil, err
		}
		vs, err := getPhysicalMonitorsFromHMONITOR(hm.Handle, n)
		if err != nil {
			return nil, err
		}
		for _, v := range vs {
			vcps = append(vcps, &v)
		}
	}
	return vcps, nil
}

type vcpWindows struct {
	Handle      windows.Handle
	Description string
}

// Info returns the monitor's capability info, parsed from its capability string.
func (vcp *vcpWindows) Info() *VCPInfo {
	capabilities, err := getCapabilitiesString(vcp.Handle)
	if err != nil {
		panic(err)
	}
	info, err := parseCabilitiesString(capabilities)
	if err != nil {
		panic(err)
	}
	return info
}

// Feature queries a monitor for a code's current and max values.
func (vcp *vcpWindows) Feature(code Code) (value, maximum uint32, err error) {
	v, m, err := getVCPFeatureAndVCPFeatureReply(vcp.Handle, byte(code))
	return uint32(v), uint32(m), err
}

// SetFeature sets a monitor's code to a value.
func (vcp *vcpWindows) SetFeature(code Code, value uint32) error {
	return setVCPFeature(vcp.Handle, byte(code), dword(value))
}

// Close tidies up after a monitor is no longer needed.
func (vcp *vcpWindows) Close() error {
	return destroyPhysicalMonitor(vcp.Handle)
}

// getNumberOfPhysicalMonitorsFromHMONITOR calls the Windows API to get the number of
// PhysicalMonitors associated with a given monitor handle.
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/physicalmonitorenumerationapi/nf-physicalmonitorenumerationapi-getnumberofphysicalmonitorsfromhmonitor
func getNumberOfPhysicalMonitorsFromHMONITOR(hm monitorHandle) (n dword, err error) {
	ret, _, err := procGetNumberOfPhysicalMonitorsFromHMONITOR.Call(
		uintptr(hm),
		uintptr(unsafe.Pointer(&n)),
	)
	if ret != validReturn {
		return 0, err
	}
	return n, nil
}

// getPhysicalMonitorsFromHMONITOR calls the Windows API to get a list of PhysicalMonitors
// associated with a given Monitor handle.
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/physicalmonitorenumerationapi/nf-physicalmonitorenumerationapi-getphysicalmonitorsfromhmonitor
func getPhysicalMonitorsFromHMONITOR(hm monitorHandle, nMonitors dword) ([]vcpWindows, error) {
	type physicalMonitor struct {
		Handle      windows.Handle
		Description [128]wchar
	}

	pms := make([]physicalMonitor, nMonitors)
	if nMonitors > 0 { // Lazy, don't bother with call for n=0
		ret, _, err := procGetPhysicalMonitorsFromHMONITOR.Call(
			uintptr(hm),
			uintptr(nMonitors),
			uintptr(unsafe.Pointer(&(pms[0]))), // Uses pointer to first object in slice
		)
		if ret != validReturn {
			return nil, err
		}
	}

	pmOut := make([]vcpWindows, nMonitors)
	for i := range pms {
		pmOut[i] = vcpWindows{
			Handle:      pms[i].Handle,
			Description: wcharToString(pms[i].Description[:]),
		}
	}
	return pmOut, nil
}

// wcharToString converts a []WCHAR ([]uint16, used in some Windows API calls) to a string.
func wcharToString(s []wchar) string {
	var b strings.Builder
	for _, c := range s {
		if c != 0 {
			b.WriteRune(rune(c))
		}
	}
	return b.String()
}

// destroyPhysicalMonitor calls the Windows API to close a PhysicalMonitor handle.
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/physicalmonitorenumerationapi/nf-physicalmonitorenumerationapi-destroyphysicalmonitor
func destroyPhysicalMonitor(pmh windows.Handle) error {
	ret, _, err := procDestroyPhysicalMonitor.Call(uintptr(pmh))
	if ret != validReturn {
		return err
	}
	return nil
}

// getCapabilitiesStringLength calls the Windows API to get the capabilities string length
// for a PhysicalMonitor handle.
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/lowlevelmonitorconfigurationapi/nf-lowlevelmonitorconfigurationapi-getcapabilitiesstringlength
func getCapabilitiesStringLength(pmh windows.Handle) (length dword, err error) {
	ret, _, err := procGetCapabilitiesStringLength.Call(
		uintptr(pmh),
		uintptr(unsafe.Pointer(&length)),
	)
	if ret != validReturn {
		return 0, err
	}
	return length, nil
}

// capabilitiesRequestAndCapabilitiesReply calls the Windows API to get the capabilities
// string for a PhysicalMonitor handle.
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/lowlevelmonitorconfigurationapi/nf-lowlevelmonitorconfigurationapi-capabilitiesrequestandcapabilitiesreply
func capabilitiesRequestAndCapabilitiesReply(pmh windows.Handle, length dword) (string, error) {
	b := make([]byte, length)
	if length > 0 {
		ret, _, err := procCapabilitiesRequestAndCapabilitiesReply.Call(
			uintptr(pmh),
			uintptr(unsafe.Pointer(&(b[0]))),
			uintptr(length),
		)
		if ret != validReturn {
			return "", err
		}
	}
	return string(b), nil
}

// getCapabilitiesString gets the capabilities string for a PhysicalMonitor handle.
func getCapabilitiesString(pmh windows.Handle) (string, error) {
	length, err := getCapabilitiesStringLength(pmh)
	if err != nil {
		return "", err
	}
	cap, err := capabilitiesRequestAndCapabilitiesReply(pmh, length)
	if err != nil {
		return "", err
	}
	return cap, nil
}

// getVCPFeatureAndVCPFeatureReply calls the Windows API to get the current and maximum
// values of a VCP
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/lowlevelmonitorconfigurationapi/nf-lowlevelmonitorconfigurationapi-getvcpfeatureandvcpfeaturereply
func getVCPFeatureAndVCPFeatureReply(pmh windows.Handle, code byte) (current dword, max dword, err error) {
	ret, _, err := procGetVCPFeatureAndVCPFeatureReply.Call(
		uintptr(pmh),
		uintptr(code),
		uintptr(0),
		uintptr(unsafe.Pointer(&current)),
		uintptr(unsafe.Pointer(&max)),
	)
	if ret != validReturn {
		return 0, 0, err
	}
	return current, max, nil
}

// setVCPFeature calls the Windows API to set the value of a VCP
//
// Reference: https://docs.microsoft.com/en-us/windows/win32/api/lowlevelmonitorconfigurationapi/nf-lowlevelmonitorconfigurationapi-setvcpfeature
func setVCPFeature(pmh windows.Handle, code byte, value dword) error {
	ret, _, err := procSetVCPFeature.Call(
		uintptr(pmh),
		uintptr(code),
		uintptr(value),
	)
	if ret != validReturn {
		return err
	}
	return nil
}
